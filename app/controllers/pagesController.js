var locomotive = require('locomotive'),
  async = require('async'),
  Controller = locomotive.Controller;

var pagesController = new Controller();
var Status = require(process.cwd()+'/app/models/' + 'status');
var Kainos = require(process.cwd()+'/app/models/' + 'kainos');
var Pages = require(process.cwd()+'/app/models/' + 'page');
var News = require(process.cwd()+'/app/models/' + 'news');

pagesController.main = function() {
  this.title = 'Locomotive';
  
  Pages.addClick();
  
  var app = this;
  async.series(
    {
      status : Status.returnStatus,
      kainos : Kainos.returnKainos,
      pages : Pages.returnPages,
      news : News.getNews
    },
    function(err, results){
      app.kainos = err || results.kainos;
      app.status = err || results.status;
      app.pages = err || results.pages;
      if(results.news)
        results.pages['info'].content[0].text = results.news.data[0].message;
      
	  app.pages = err || results.pages;
      app.render();
    }
  );
  
}

/*pagesController.edit = function() {
  this.title = 'edit';
  this.test = 'test';
  var app = this;
  async.series(
    {
      status : Status.returnStatus,
      kainos : Kainos.returnKainos,
      pages : Pages.returnPages,
      news : News.getNews
    },
    function(err, results){
      app.kainos = err || results.kainos;
      app.status = err || results.status;
      app.pages = err || results.pages;
      if(results.news)
      	results.pages['info'].content[0].text = results.news.data[0].message;
      
	  app.pages = err || results.pages;
      app.render();
    }
  );
}

pagesController.editPost = function() {
  	var post = this.req.body;
    this.res.send(post.test || "vo bybi");
}*/

module.exports = pagesController;