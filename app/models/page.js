var mongoose = require('mongoose');
var pagesSchema = new mongoose.Schema({}, {strict : false});


var model = {
  //sets default mongoose model
  Pages : mongoose.model('Pages', pagesSchema),
  
  returnPages : function(callback){
    model.Pages.find({}, function(err, data) {
      var temp = {};
      for(var i in data)
      	temp[data[i].type] = data[i];
      delete data;
      
      callback(err, temp);
    }).lean();
  },
  addClick : function(){
    
    var date = new Date();
    var dd = date.getDate();
    var mm = date.getMonth()+1;
    var yyyy = date.getFullYear();
    if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} date = yyyy+'-'+mm+'-'+dd;
    var obj = {}; obj[date] = 1;
    
    model.Pages.update({type : 'visitors'}, {$inc: obj}, {upsert: true},function(err, data){});
  }
  
}


module.exports = model;