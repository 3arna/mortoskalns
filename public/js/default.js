jQuery(function($){
  var windowProps = {
    w : $('body').width(),
    h : $(window).height(),
    a : $('body').height()
  }
  
  var active = false;
  var popup = $('.popup');
  var fog = $('.fog');
  
  function absoluteCenter(element){
      element.css({'left':(windowProps.w/2)-(element.width()/2), 'top':(windowProps.h/2)-(element.height()/2)});
  }
  
  $('.gallery img').click(function(){
    var src = $(this).attr('src');
    
    
    popup.find('#container').html('<img src="'+ src +'"/>');
    absoluteCenter(popup);
    popup.show();
    fog.css({'height' : windowProps.a});
    fog.show();
  })
  
  $('.fog, .close').click(function(){
    fog.hide();
    popup.hide();
  })
  
 
  $(".scroll").click(function(event){
    
    if(!active){
      $("html, body").animate({ scrollTop: $( $(this).attr('href') ).offset().top-44 }, 600);active=true;
    
      setTimeout(function() {
          active = false;
      }, 600);
    }
    return false;
  });
  
});

